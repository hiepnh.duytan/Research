/*
 * k_modes.cpp
 *
 *  Created on: Jul 12, 2017
 *      Author: Administrator
 */

#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include "helper.h"
#include "k_modes.h"

using namespace std;

///////////////////
int main(int argc, char* args[]) {

	int N = 47;
	int M = 35;
	int K = 4;
	int T = 20;
	string path = "data/";
	string file_name = "soybean-small.data";

	// COMMAND-LINE <N> <M> <K> <T> <file_name>
	cout<<"k-modes-max-min - C++\n";
	if(argc > 1)
		N = stoi(args[1]);
	if(argc > 2)
		M = stoi(args[2]);
	if(argc > 3)
		K = stoi(args[3]);
	if(argc > 4)
		T = stoi(args[4]);
	if(argc > 5)
		file_name = string(args[5]);
	cout<<"N = "<<N<<endl;
	cout<<"M = "<<M<<endl;
	cout<<"K = "<<K<<endl;
	cout<<"#iterations = "<<T<<endl;
	cout<<"file_name = "<<file_name<<endl;

	//
//	int** X = load_data(path, file_name, N, M);
	int** X = load_data_arff(path, file_name, N, M);
	int* GT = true_membership(X, N, M);
//	print_data(X, N, M);

	__int64 start = Timer::get_millisec();

	int** Q = init_modes_max_min(X, N, M, K);
	cout<<"initial Q:"<<endl;
	print_array(Q, K, M);

	// find nearest cluster id of modes in Q
	find_nearest_cluster_GT(X, Q, N, M, K);

	//
	int* W = k_modes(X, Q, N, M, K, T, true);
	cout<<"Q final:"<<endl;
	print_array(Q, K, M);

	cout<<"Rand Index(GT,W) = "<<metric_rand_index_fast(GT, W, N, K)<<endl;
	int** C = new int*[K];
	for (int i = 0; i < K; i++)
		C[i] = new int[K];
	metric_confusion_matrix(GT, W, N, K, C, true);
	print_matrix(C, K);
	vector<double> ret = metric_accuracy_precision_recall(C, N, K);
	cout<<"Accuracy = "<<ret[0]<<endl;
	cout<<"Precision = "<<ret[1]<<endl;
	cout<<"Recall = "<<ret[2]<<endl;
	cout<<"metrics, elapsed : "<< (Timer::get_millisec() - start) <<endl;
	//
//	find_best_permutation(GT, W, N, K);

	cout<<"k_modes - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

//	cout<<"modularity_and_cluster_cost:"<<endl;
//	modularity_and_cluster_cost(X, GT, W, N, M, K, edge_threshold);
	//
	return 0;
}



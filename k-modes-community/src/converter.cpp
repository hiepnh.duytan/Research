/*
 * converter.cpp
 *
 *  Created on: Jul 13, 2017
 *      Author: Administrator
 *      - convert string attributes to integers
 *      - remove singleton attributes
 *  Oct 18:
 *  	- normalize_attribute_values() to avoid error in dp-cluster-cpp\dp_kmodes.cpp (dp_synopsis)
 *  Oct 23:
 *  	- convert_arff(): .data to .arff for kModes-init-Khan (paper: Cluster center initialization algorithm for K-modes clustering, 2013)
 *  Oct 27:
 *  	- convert_numeric_CHESS()
 *  	- convert_numeric_HEART()
 */

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "helper.h"
#include "k_modes.h"

using namespace std;

////
vector<map<string, int>> read_maps(string path, string file_name, int N, int M){
	vector<map<string, int>> maps;
	for (int j = 0; j < M+1; j++)
		maps.push_back(map<string, int>());

	//
	ifstream f(path + file_name);

	string line;
	vector<int> max_id(M+1);
	for (int i = 0; i < N; i++){
		getline(f, line);
		vector<string> items = Formatter::split(line, ',');
		for (int j = 0; j < M+1; j++)
			if (maps[j].count(items[j]) == 0){
				maps[j][items[j]] = max_id[j];
				max_id[j] += 1;
			}

	}
	f.close();

	//
	return maps;
}

//// remove singleton attributes
int** remove_singleton(int** X, int N, int M, int &MY){
	int** Y = new int*[N];

	// 1 - remove singleton columns
	vector<vector<pair_int>> counter = attr_stats(X, N, M);

	vector<int> attr;	// ids of non-singleton attributes
	for (int j = 0; j < M; j++)
		if (counter[j].size() > 1)
			attr.push_back(j);

	MY = attr.size();
	cout<<"MY = "<<MY<<endl;

	for (int i = 0; i < N; i++){
		Y[i] = new int[MY+1];
		for (int j = 0; j < MY; j++)
			Y[i][j] = X[i][attr[j]];
		Y[i][MY] = X[i][M];
	}

	// 2 - normalize attribute values to 0,1,..|Aj|-1


	//
	return Y;
}

//// X[i][M] in [0..K-1]
void normalize_cluster_index(int** X, int N, int M){
	map<int, int> idx;
	int id = 0;
	for (int i = 0; i < N; i++)
		if (idx.count(X[i][M]) == 0){
			idx[X[i][M]] = id;
			id++;
		}
	//
	for (int i = 0; i < N; i++)
		X[i][M] = idx[X[i][M]];

}

//// X[i][M] in [0..K-1]
void normalize_attribute_values(int** X, int N, int M){

	for (int j = 0; j < M; j++){
		int min_val = 1000000;
		for (int i = 0; i < N; i++)
			min_val = min_val > X[i][j] ? X[i][j] : min_val;

		for (int i = 0; i < N; i++)
			X[i][j] = X[i][j] - min_val;
	}

}


////
void save_data(string path, string out_file, int** Y, int N, int M){

	string file_name = path + out_file;

	ofstream fout(file_name, ofstream::out);
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++)
			fout<<Y[i][j]<<",";
		fout<<Y[i][M]<<endl;
	}

	fout.close();
}

//// convert string attributes to integers
int** convert_numeric_MUSHROOM(){
	string path = "data/";
	string file_name = "mushroom.data";
	int N = 8124;
	int M = 22;
	vector<map<string, int>> maps = read_maps(path, file_name, N, M);

	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = maps[j+1][items[j+1]];
		X[i][M] = maps[0][items[0]];			// class label in the 1st column!

		i++;
	}
	f.close();

	//
	save_data("data/", "mushroom-converted.data", X, N, M);

	//
	return X;
}

//// convert string attributes to integers
int** convert_numeric_ZOO(){
	string path = "data/";
	string file_name = "zoo.data";
	int N = 101;
	int M = 16;
	vector<list<string>> names = {{"aardvark","antelope","bear","boar","buffalo","calf","cavy","cheetah","deer","dolphin","elephant",
			"fruitbat","giraffe","girl","goat","gorilla","hamster","hare","leopard","lion","lynx","mink","mole","mongoose","opossum",
			"oryx","platypus","polecat","pony","porpoise","puma","pussycat","raccoon","reindeer","seal","sealion","squirrel","vampire",
			"vole","wallaby","wolf"},
			{"chicken","crow","dove","duck","flamingo","gull","hawk","kiwi","lark","ostrich","parakeet","penguin","pheasant","rhea",
			"skimmer","skua","sparrow","swan","vulture","wren"},
			{"pitviper","seasnake","slowworm","tortoise","tuatara"},
			{"bass","carp","catfish","chub","dogfish","haddock","herring","pike","piranha","seahorse","sole","stingray","tuna"},
			{"frog","frog","newt","toad"},
			{"flea","gnat","honeybee","housefly","ladybird","moth","termite","wasp"},
			{"clam","crab","crayfish","lobster","octopus","scorpion","seawasp","slug","starfish","worm"},
	};


	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M+1; j++)
			X[i][j] = stoi(items[j+1]);		// class label in the last column!
//		for (int t = 0; t < names.size(); t++)
//			for (string str : names[t])
//				if (str.compare(items[0]) == 0){
//					X[i][M] = t;			// class label in the 1st column!
//					break;
//				}

		i++;
	}
	f.close();

	normalize_cluster_index(X, N, M);

	//
	save_data("data/", "zoo-converted.data", X, N, M);

	//
	return X;
}

////
int** convert_numeric_LUNG_CANCER(){
	string path = "data/";
	string file_name = "lung-cancer.data";
	int N = 32;
	int M = 56;


	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = stoi(items[j+1]);
		X[i][M] = stoi(items[0]);

		i++;
	}
	f.close();
	//
	normalize_cluster_index(X, N, M);

	normalize_attribute_values(X, N, M);

	//
	save_data("data/", "lung-cancer-converted.data", X, N, M);

	//
	return X;
}

////
int** convert_numeric_BREAST_CANCER(){
	string path = "data/";
	string file_name = "breast-cancer.data";
	int N = 699;
	int M = 9;


	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M+1; j++)
			X[i][j] = stoi(items[j+1]);		// class label in the last column!

		i++;
	}
	f.close();
	//
	normalize_cluster_index(X, N, M);

	//
	save_data("data/", "breast-cancer-converted.data", X, N, M);

	//
	return X;
}

////
int** convert_numeric_DERMATOLOGY(){
	string path = "data/";
	string file_name = "dermatology.data";
	int N = 366;
	int M = 34;


	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j <= M; j++)
			X[i][j] = stoi(items[j]);		// class label in the last column!

		i++;
	}
	f.close();
	// age column
	for (int i = 0; i < N; i++)
		X[i][33] = (X[i][33]-1) / 10 + 1;
	//
	normalize_cluster_index(X, N, M);

	//
	save_data("data/", "dermatology-converted.data", X, N, M);

	//
	return X;
}

////
int** convert_numeric_VOTE(){
	string path = "data/";
	string file_name = "vote.data";
	int N = 435;
	int M = 16;

	map<string, int> classes = {{"republican",0}, {"democrat",1}};
	map<string, int> attrs = {{"?",0}, {"n",1}, {"y",2}};

	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = attrs[items[j+1]];		// class label in the 1st column!
		X[i][M] = classes[items[0]];
		i++;
	}
	f.close();
	//
	normalize_cluster_index(X, N, M);

	//
	save_data("data/", "vote-converted.data", X, N, M);

	//
	return X;
}

//// convert string attributes to integers
int** convert_numeric_CHESS(){
	string path = "data/";
	string file_name = "chess.data";
	int N = 3196;
	int M = 36;
	vector<map<string, int>> maps = read_maps(path, file_name, N, M);
	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];
	//
	ifstream f(path + file_name);

	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = maps[j+1][items[j+1]];
		X[i][M] = maps[0][items[0]];			// class label in the 1st column!

		i++;
	}
	f.close();

	//
	save_data("data/", "chess-converted.data", X, N, M);

	//
	return X;
}

//// convert real attributes to integers
int** convert_numeric_HEART(){
	string path = "data/";
	string file_name = "heart-cleveland.data";
	int N = 303;
	int M = 13;
	//
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];

	double** Xorg = new double*[N];
	for (int i = 0; i < N; i++)
		Xorg[i] = new double[M+1];
	// read double values
	ifstream f(path + file_name);

	string line;
	for (int i = 0; i < N; i++){
		getline(f, line);
		vector<string> items = Formatter::split(line, ',');
		for (int j = 0; j < M+1; j++)
			Xorg[i][j] = stod(items[j]);

	}
	f.close();

	// copy categorical attributes
	int n_cat = 9;
	int cat_attr[] = {1,2,5,6,8,10,11,12,13};
	for (int j = 0; j < n_cat; j++){
		for (int i = 0; i < N; i++)
			X[i][cat_attr[j]] = int(Xorg[i][cat_attr[j]]);
	}

	// discretize
	int n_numeric = 5;
	int numeric_attr[] = {0,3,4,7,9};
	double numeric_interval[] = {10,20,60,30,0.7};
	for (int j = 0; j < n_numeric; j++){
		for (int i = 0; i < N; i++)
			X[i][numeric_attr[j]] = int(Xorg[i][numeric_attr[j]] / numeric_interval[j]);
	}

	//
	save_data("data/", "heart-converted.data", X, N, M);

	//
	return X;
}

////
void convert_arff(){
	string path = "data/";
	vector<string> file_arr = {"soybean-small-mod", "mushroom-converted", "zoo-converted", "lung-cancer-converted", "breast-cancer-converted",
			"dermatology-converted", "vote-converted", "nursery-converted", "chess-converted", "heart-converted"};
	vector<int> N_arr = {47, 8124, 101, 32, 699, 366, 435, 12960, 3196, 303};
	vector<int> M_arr = {21, 22, 16, 56, 9, 34, 16, 8, 36, 13};
	vector<int> K_arr = {4, 2, 7, 3, 2, 6, 2, 5, 2, 5};

	for (int t = 0; t < file_arr.size(); t++){
		string infile = file_arr[t] + ".data";
		string outfile = path + file_arr[t] + ".arff";
		int N = N_arr[t];
		int M = M_arr[t];
		int K = K_arr[t];

		//
		int** X = load_data(path, infile, N, M);

		vector<vector<pair_int>> attr_counter = attr_stats(X, N, M);

		// write to file
		ofstream fout(outfile, ofstream::out);
		fout<<"@relation "<<file_arr[t]<<endl;
		for (int j = 0; j < M; j++){
			int max_val = 0;
			for (int v = 0; v < attr_counter[j].size(); v++)
				max_val = max_val < attr_counter[j][v].key ? attr_counter[j][v].key : max_val;
			max_val += 1;

			fout<<"@attribute A"<<j<<" {";
			for (int v = 0; v < max_val-1; v++)
				fout<<"'"<<v<<"',";
			fout<<"'"<<max_val-1<<"'}"<<endl;
		}
		// classes
		fout<<"@attribute Class {";
		for (int v = 0; v < K-1; v++)
			fout<<"'"<<v<<"',";
		fout<<"'"<<K-1<<"'}"<<endl;

		// data
		fout<<"@data"<<endl;

		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++)
				fout<<"'"<<X[i][j]<<"',";
			fout<<"'"<<X[i][M]<<"'"<<endl;
		}

		fout.close();
	}

}


///////////////////
int main(int argc, char* args[]) {

	int N = 47;
	int M = 35;
	string path = "data/";
	string file_name = "soybean-small.data";
	string out_name = "soybean-small-mod.data";

	// COMMAND-LINE <N> <M> <file_name> <out_name>
//	cout<<"converter - C++\n";
//	if(argc > 1)
//		N = stoi(args[1]);
//	if(argc > 2)
//		M = stoi(args[2]);
//	if(argc > 3)
//		file_name = string(args[3]);
//	if(argc > 4)
//		out_name = string(args[4]);
//	cout<<"N = "<<N<<endl;
//	cout<<"M = "<<M<<endl;
//	cout<<"file_name = "<<file_name<<endl;
//	cout<<"out_name = "<<out_name<<endl;
//
//	//
//	int** X = load_data(path, file_name, N, M);
//	int MY = 0;
//	int** Y = remove_singleton(X, N, M, MY);
//	normalize_cluster_index(Y, N, MY);
//
//	save_data(path, out_name, Y, N, MY);

	//
//	convert_numeric_MUSHROOM();
//	convert_numeric_ZOO();
//	convert_numeric_LUNG_CANCER();
//	convert_numeric_BREAST_CANCER();
//	convert_numeric_DERMATOLOGY();
//	convert_numeric_VOTE();
	// NURSERY --> see private-cluster (Python)
//	convert_numeric_CHESS();
//	convert_numeric_HEART();

	////
	convert_arff();

	//
	return 0;
}



/*
 * dist_stats.cpp
 *
 *  Created on: Aug 12, 2017
 *      Author: Administrator
 *  Aug 29:
 *  	- add intra_dist_distr()
 *  Nov 1:
 *  	- add inter_dist_distr()
 */


#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include "helper.h"
#include "k_modes.h"
#include "matio_private.h"

using namespace std;

////
double* dist_distr(int** X, int N, int M){
	double* D = new double[M+1];
	for (int j = 0; j < M+1; j++)
		D[j] = 0;

	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++){
			int d = dist(X[u], X[v], M);
			D[d] += 1;
		}
	//
	return D;
}

//// cluster_dist: dist distribution to mode for each cluster
double** cluster_dist_distr(int** X, int N, int M, int K){
	double** cluster_dist;

	cluster_dist = new double*[K];
	for (int k = 0; k < K; k++){
		cluster_dist[k] = new double[M+1];
		for (int d = 0; d <= M; d++)
			cluster_dist[k][d] = 0;
	}

	for (int k = 0; k < K; k++){
		vector<int> idx;
		for (int i = 0; i < N; i++)
			if (X[i][M] == k)
				idx.push_back(i);
		//
		int* q = find_mode(X, idx, M);

		for (int i : idx){
			int d = dist(q, X[i], M);
			cluster_dist[k][d] += 1;
		}

	}
	//
	return cluster_dist;
}

//// intra_dist: intra-cluster dist distribution for each cluster
double** intra_dist_distr(int** X, int N, int M, int K){
	double** intra_dist;

	intra_dist = new double*[K];
	for (int k = 0; k < K; k++){
		intra_dist[k] = new double[M+1];
		for (int d = 0; d <= M; d++)
			intra_dist[k][d] = 0;
	}

	for (int k = 0; k < K; k++){
		vector<int> idx;
		for (int i = 0; i < N; i++)
			if (X[i][M] == k)
				idx.push_back(i);
		//
		for (int u = 0; u < idx.size(); u++)
			for (int v = u+1; v < idx.size(); v++){
				int d = dist(X[idx[u]], X[idx[v]], M);
				intra_dist[k][d] += 1;
			}

	}
	//
	return intra_dist;
}

//// inter_dist: inter-cluster dist distribution
double* inter_dist_distr(int** X, int N, int M){
	double* inter_dist;

	inter_dist = new double[M+1];
	for (int d = 0; d <= M; d++)
		inter_dist[d] = 0;

	for (int u = 0; u < N; u++)
		for (int v = 0; v < N; v++)
			if (X[u][M] != X[v][M]){
				int d = dist(X[u], X[v], M);
				inter_dist[d] += 1;
			}
	//
	return inter_dist;
}

////
void write_matlab(string output_name, double* dist, double** cluster_dist, double** intra_dist, double* inter_dist, int M, int K){

	mat_t *mat = Mat_CreateVer(output_name.c_str(), NULL, MAT_FT_DEFAULT);
	matvar_t *matvar;

	size_t dim_dist[1] = {M+1};				// length(dist) = M+1 (from 0 to M)
	size_t dim_cluster_dist[2] = {K,M+1};

	// must be 1d array, column-first
	double c_dist[K*(M+1)];
	for (int d = 0; d <= M; d++)
		for (int k = 0; k < K; k++)
			c_dist[d * K + k] = cluster_dist[k][d];

	double i_dist[K*(M+1)];
	for (int d = 0; d <= M; d++)
		for (int k = 0; k < K; k++)
			i_dist[d * K + k] = intra_dist[k][d];

	//
	matvar = Mat_VarCreate("dist", MAT_C_DOUBLE, MAT_T_DOUBLE, 1, dim_dist, dist, 0);
	Mat_VarWrite(mat, matvar, MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);
	//
	matvar = Mat_VarCreate("cluster_dist", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim_cluster_dist, c_dist, 0);
	Mat_VarWrite(mat, matvar, MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);
	//
	matvar = Mat_VarCreate("intra_dist", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim_cluster_dist, i_dist, 0);
	Mat_VarWrite(mat, matvar, MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);
	//
	matvar = Mat_VarCreate("inter_dist", MAT_C_DOUBLE, MAT_T_DOUBLE, 1, dim_dist, inter_dist, 0);
	Mat_VarWrite(mat, matvar, MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);
	//
	Mat_Close(mat);
}

///////////////////
int main(int argc, char* args[]) {

	int N = 47;
	int M = 21;
	int K = 4;
	string path = "data/";
	string file_name = "soybean-small-mod.data";
	string dist_file = "matlab/soybean-small-mod.csv";

	// COMMAND-LINE <N> <M> <K> <file_name>
	cout<<"dist_stats - C++\n";
	if(argc > 1)
		N = stoi(args[1]);
	if(argc > 2)
		M = stoi(args[2]);
	if(argc > 3)
		K = stoi(args[3]);
	if(argc > 4)
		file_name = string(args[4]);
	cout<<"N = "<<N<<endl;
	cout<<"M = "<<M<<endl;
	cout<<"K = "<<K<<endl;
	cout<<"file_name = "<<file_name<<endl;
	dist_file = "matlab/" + file_name.substr(0, file_name.size()-5) + ".mat"; 	// ".csv";
	cout<<"dist_file = "<<dist_file<<endl;

	//
	int** X = load_data(path, file_name, N, M);
//	print_data(X, N, M);
//	print_stats(X, N, M);

	//
	double* D = dist_distr(X, N, M);
	double** CD = cluster_dist_distr(X, N, M, K);
	double** intraD = intra_dist_distr(X, N, M, K);
	double* interD = inter_dist_distr(X, N, M);

	write_matlab(dist_file, D, CD, intraD, interD, M, K);

	//
	return 0;
}


soybean ------------------
sum(C) = 47
assignment : 0 1 3 2 
Confusion matrix
10	0	0	0	
0	10	0	0	
0	0	10	1	
0	0	0	16	
Accuracy = 0.978723
Precision = 0.977273
Recall = 0.985294
mushroom ------------------
sum(C) = 8124
assignment : 0 1 
Confusion matrix
2573	48	
1343	4160	
Accuracy = 0.828779
Precision = 0.868819
Recall = 0.822821
zoo ------------------
sum(C) = 101
assignment : 6 5 3 4 1 2 0 
Confusion matrix
35	0	0	0	0	0	0	
0	13	0	0	0	0	1	
0	0	20	0	0	0	0	
0	0	0	7	0	0	0	
0	0	0	2	8	0	0	
6	0	0	0	0	0	0	
0	0	0	1	0	4	4	
Accuracy = 0.861386
Precision = 0.739002
Recall = 0.764808
lung ------------------
sum(C) = 32
assignment : 2 1 0 
Confusion matrix
3	2	2	
3	4	1	
3	7	7	
Accuracy = 0.4375
Precision = 0.446779
Recall = 0.447009
breast ------------------
sum(C) = 699
assignment : 0 1 
Confusion matrix
390	189	
68	52	
Accuracy = 0.632332
Precision = 0.553454
Recall = 0.533648
dermatology ------------------
sum(C) = 366
assignment : 5 2 0 1 3 4 
Confusion matrix
46	9	43	8	22	0	
0	95	0	0	0	0	
14	2	20	9	14	0	
0	0	0	34	0	0	
1	6	9	1	13	2	
0	0	0	0	0	18	
Accuracy = 0.617486
Precision = 0.684101
Recall = 0.61654
vote ------------------
sum(C) = 435
assignment : 0 1 
Confusion matrix
158	55	
10	212	
Accuracy = 0.850575
Precision = 0.848369
Recall = 0.867242
nursery ------------------
sum(C) = 12960
assignment : 4 0 3 1 2 
Confusion matrix
830	0	32	745	883	
465	0	15	350	565	
654	0	54	765	489	
885	2	125	1143	500	
1486	0	102	1263	1607	
Accuracy = 0.280401
Precision = 0.230368
Recall = 0.204415
chess ------------------
sum(C) = 3196
assignment : 0 1 
Confusion matrix
2107	262	
684	143	
Accuracy = 0.704005
Precision = 0.531159
Recall = 0.554006
heart ------------------
sum(C) = 303
assignment : 3 0 2 1 4 
Confusion matrix
87	11	4	2	1	
24	13	1	3	1	
9	9	13	8	5	
12	19	18	21	7	
33	1	0	1	0	
Accuracy = 0.442244
Precision = 0.341255
Recall = 0.346733

% plot distance CDF

% syn_N_M_K_maxd_minsize_maxsize

data_list = {'soybean-small-mod', 'mushroom-converted', 'zoo-converted', 'lung-cancer-converted', 'breast-cancer-converted', 'dermatology-converted', 'vote-converted', ...
    'nursery-converted', 'chess-converted', 'heart-converted'};
title_list = {'soybean-small', 'mushroom', 'zoo', 'lung-cancer', 'breast-cancer', 'dermatology', 'vote', 'nursery', 'chess', 'heart'};
N_list = [47, 8124, 101, 32, 699, 366, 435, 12960, 3196, 303];
M_list = [21, 22, 16, 56, 9, 34, 16, 8, 36, 13];
K_list = [4, 2, 7, 3, 2, 6, 2, 5, 2, 5];


% D = csvread('../soybean-small-mod.csv');          K = 4; % r = 8
% D = csvread('../mushroom-converted.csv');         K = 2; % r = 12
% D = csvread('../zoo-converted.csv');              K = 7; % r = 3
% D = csvread('../lung-cancer-converted.csv');      K = 3; % r = 23
% D = csvread('../breast-cancer-converted.csv');    K = 2; % r = 6
% D = csvread('../dermatology-converted.csv');      K = 6; % r = 11
% D = csvread('../vote-converted.csv');             K = 2; % r = 8

% D = csvread('../syn_8510_30_30_5_100_500.csv');
% D = csvread('../syn_8510_20_30_20_100_500.csv');

pos = 10;

load(['../' data_list{pos} '.mat']);
% D = csvread(['../' data_list{pos} '.csv']);
D = dist;
D = cumsum(D);
D = D/D(length(D));

% search r for the given K
K = K_list(pos);
invK = 1/K;
for r = 1:length(D)
    if D(r) >= invK
        break;
    end
end
fprintf('r = %d\n', r-1);

%
x0=400;
y0=200;
width=600;
height=400;
paper_size = [0 0 15 6];
font_size=20;

plot(0:length(D)-1, D, '-x');
line([0 M_list(pos)], [invK invK]);
axis([0 M_list(pos) 0 1]);
h=title(title_list{pos}); set(h,'FontSize', font_size);
h=xlabel('Hamming distance'); set(h,'FontSize', font_size);
h=ylabel('CDF'); set(h,'FontSize', font_size);
set(gcf,'units','points','position',[x0,y0,width,height], 'PaperPosition', paper_size)
set(gca,'FontSize', font_size)



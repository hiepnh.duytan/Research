
clear;

data_list = {'soybean-small-mod', 'mushroom-converted', 'zoo-converted', 'lung-cancer-converted', 'breast-cancer-converted', 'dermatology-converted', 'vote-converted',...
    'nursery-converted', 'chess-converted', 'heart-converted'};
title_list = {'soybean-small', 'mushroom', 'zoo', 'lung-cancer', 'breast-cancer', 'dermatology', 'vote', 'nursery', 'chess', 'heart'};
N_list = [47, 8124, 101, 32, 699, 366, 435, 12960, 3196, 303];
M_list = [21, 22, 16, 56, 9, 34, 16, 8, 36, 13];
K_list = [4, 2, 7, 3, 2, 6, 2, 5, 2, 5];
R_list = [7, 11, 2, 22, 6, 10, 8, 3, 9, 5];

pos = 10;

load(['../' data_list{pos} '.mat']);

N = N_list(pos);
M = M_list(pos);
K = K_list(pos);
R = R_list(pos);

dist_values = (0:M);

avg_intra_dist = sum(sum(intra_dist.*repmat(dist_values, K, 1))) / sum(sum(intra_dist));

avg_inter_dist = sum(inter_dist'.*dist_values) / sum(inter_dist);